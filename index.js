const buttons = document.querySelectorAll(".bracket, .operator, .number, .dot")
const display = document.querySelector(".display")
const equalsButton = document.querySelector(".equals")
const clearButton = document.querySelector(".clear")
const operators = document.querySelectorAll(".operator")
const operationMenu = document.querySelector(".operation")
const resultMenu = document.querySelector(".result")


let displayData = ""
let runEngine = true

buttons.forEach(button => {
    button.addEventListener('click', () => {
        const buttonValue = button.getAttribute("button-data")
        displayData += buttonValue
        operationMenu.textContent = displayData
    })
})

equalsButton.addEventListener("click", () => {
    operationMenu.textContent = displayData + "="
    if (displayData !== "") {

        try {
            displayData = eval(displayData)
            console.log(operationMenu.classList);
            // operationMenu.classList.add("button-equal-clicked")
            resultMenu.textContent = displayData
        }

        catch(err) {
            clearDisplay()
            resultMenu.textContent = "Operation not supported"
            resultMenu.classList.add("error-message")

            document.querySelectorAll(".bracket, .number, .operator, .equals, .dot").forEach(disabledButton => {
                disabledButton.disabled = true
            
                clearButton.addEventListener("click", () => {
                    displayData = ""
                    operationMenu.textContent = displayData
                    resultMenu.textContent = displayData
                    disabledButton.disabled = false
                })
            })
        }
    }

})


clearButton.addEventListener("click", () => {
    displayData = ""
    operationMenu.textContent = displayData
    resultMenu.textContent = displayData
    resetButtons()
})


function clearDisplay () {
    operationMenu.textContent = ""
    resultMenu.textContent = ""
}

function resetButtons () {
    buttons.forEach(button => {
        button.disabled = false
    })
    equalsButton.disabled = false
}

function disableOperators (displayData="") {
    if (runEngine){
        buttons.forEach(button => {
            button.classList.forEach(buttonClassName => {
                if (displayData === "") {
                    equalsButton.disabled = true
                    if (buttonClassName == "operator") {
                        button.disabled = true
                    }
                }
            })
        })
    }
}
